export function validateEmail (checkThis) {
  let rxCheck = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  let bolCheck = rxCheck.test(checkThis)
  return bolCheck
}

//  ### HOW TO USE ###
//  import {validateEmail} from '<..path>/validate-email.js'
//    in your code, do something like:
//    var check = validateEmail(email_address_to_check)
//    RESULT: check will be 'true' if the email_address_to_check is 'valid'. Otherwise check will be 'false'.

//  ### WHO TO THANK ###
// Big Shout Out to http://stackoverflow.com/users/3518452/rnevius
//  http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
