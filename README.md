Itsy bitsy JavaScript code snippets. Helper scripts.

### HOW TO USE ###
Include the following line in your JavaScript file:  
    `import {FUNCTION_NAME} from '<..path>/helper_file_name>.js'`


In your code, do something like (depending on the documentation in the helper):  
    `var check = FUNCTION_NAME(ARGUMENTS)`